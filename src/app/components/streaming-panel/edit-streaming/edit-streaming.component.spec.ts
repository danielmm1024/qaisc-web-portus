import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditStreamingComponent } from './edit-streaming.component';

describe('EditStreamingComponent', () => {
  let component: EditStreamingComponent;
  let fixture: ComponentFixture<EditStreamingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditStreamingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditStreamingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
