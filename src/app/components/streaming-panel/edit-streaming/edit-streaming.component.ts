import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Stream } from 'src/app/models/stream';
import { StreamService } from 'src/app/services/stream.service';

@Component({
  selector: 'app-edit-streaming',
  templateUrl: './edit-streaming.component.html',
  styleUrls: ['./edit-streaming.component.scss'],
})
export class EditStreamingComponent implements OnInit {
  id: any;
  currentStream: Stream;
  streamingForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private streamService: StreamService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    // this.getCamera(this.id);
    this.streamService.getStreamEdit(this.id).subscribe((stream: Stream) => {
      this.currentStream = stream;
      this.currentStream.data = JSON.parse(stream.data)
    });
    this.streamingForm = this.createFormGroup();
  }
  createFormGroup() {
    return this.formBuilder.group({
      protocol: new FormControl('', [Validators.required]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
      url: new FormControl('', [Validators.required, Validators.minLength(4)]),
      user: new FormControl(''),
      password: new FormControl(''),
      video_url: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
    });
  }

  getCamera(id: string) {
    this.streamService.getStream(this.id).subscribe((stream: Stream) => {
      this.currentStream = stream;
      this.currentStream.data = JSON.parse(stream.data)
    });
  }

  updateStream() {
    this.streamService
      .updateStream(this.currentStream)
      .subscribe((res: any) => { });
    this.router.navigate(['streaming-dashboard/' + this.currentStream.camera_id]);
  }

  closeForm() {
    this.router.navigate(['cameras']);
  }

  get form() {
    return this.streamingForm.controls;
  }
}
