import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MediaRecorder } from 'extendable-media-recorder';
import { ActivatedRoute, Router } from '@angular/router';
import { CameraService } from 'src/app/services/camera.service';
import { StreamService } from 'src/app/services/stream.service';
import { OpenStackService } from 'src/app/services/open-stack.service';
import { Camera } from 'src/app/models/camera';
import { Stream } from 'src/app/models/stream';
import { Image } from 'src/app/models/image';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MatMenuTrigger } from '@angular/material/menu';
import { Position } from 'src/app/models/position';
import Swal from 'sweetalert2'
import { Timelapse } from 'src/app/models/timelapse';
declare function unLoadWebRtc(): any;

@Component({
  selector: 'app-streaming-panel',
  templateUrl: './streaming-panel.component.html',
  styleUrls: ['./streaming-panel.component.scss'],
})
export class StreamingPanelComponent implements OnInit, AfterViewInit {
  recordedBlobs: Blob[];
  imageBlobs: Blob[];
  mediaRecorder: any;
  @ViewChild('recorded') recorded: ElementRef;
  downloadUrl: any;
  isRecording: boolean = false;
  isFinishRecord: boolean = false;
  isStopped: boolean = true;
  base64: any;
  base64data: any;
  value: any;

  v = 0.4;
  id: any;
  url: any;
  video_url: any;
  currentCamera: Camera;
  setTimeoutId: any;

  currentImage: Image;
  images: any[];
  defaultTime = '0';
  urlImages: any[] = [];
  lazyLoadingInterval: any;

  timelapses: Timelapse[];
  positions: Position[];
  positionName: string;
  positionX: any
  positionY: any
  positionZ: any

  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;

  constructor(
    private route: ActivatedRoute,
    private cameraService: CameraService,
    private streamService: StreamService,
    private openStackService: OpenStackService,
    private router: Router,
    private modalService: NgbModal,
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.cameraService.getCamera(this.id).subscribe((camera: Camera) => {
      this.currentCamera = camera;
    });
    this.streamService.getStream(this.id).subscribe((stream: Stream) => {
      this.streamService.loadStreaming(stream);
    });
    this.streamService.getTimelapses(this.id).subscribe((timelapses: Timelapse[]) => {
      this.timelapses = timelapses
    })
    this.streamService.getPositions(this.id).subscribe((positions: Position[]) => {
      this.positions = positions
    })
    // this.openStackService.getImagesByScene(20).subscribe((images: Image[]) => {
    //   this.images = Object.values(images);
    // });
  }

  ngAfterViewInit(): void {
    // setInterval(() => this.takePicture(), 60000);
  }

  openCameraPanel() {
    this.router.navigate(['cameras']);
  }

  close() {
    if (this.isRecording) {
      Swal.fire({
        icon: 'error',
        text: 'Estas grabando un video!!'
      })
    } else {
      unLoadWebRtc();
      this.router.navigate([''])
    }
  }

  openModal(content: any, task_id: number) {
    this.showLoading()
    this.openStackService.getImagesByScene(task_id).subscribe((images: Image[]) => {
      this.images = Object.values(images);
      this.urlImages = [];
      this.setCurrentImage(this.images[0])
      Swal.close()
      this.urlImages.push(...this.images.splice(0, 6));
      setTimeout(() => {
        this.modalService.open(content, { size: 'xl' }).result.then((result) => {
          this.closeModal()
        }, (reason) => {
          this.closeModal()
        }
        );
        this.lazyLoadingImages();
      }, 200)
    });
  }

  closeModal() {
    this.modalService.dismissAll();
    clearInterval(this.lazyLoadingInterval);
  }

  lazyLoadingImages() {
    this.lazyLoadingInterval = setInterval(() => {
      this.urlImages.push(...this.images.splice(0, 6))
      if (this.images.length == 0) {
        clearInterval(this.lazyLoadingInterval)
      }
    }, 1000)
  }

  public showImage(image: any) {
    this.setCurrentImage(image);
  }

  setCurrentImage(image: any) {
    this.currentImage = {
      name: image.name,
      id: image.id,
      camera: image.camera,
      container: image.container,
      hash: image.hash,
      taken_at: image.created_at,
    };
  }

  public showLoading() {
    Swal.fire({
      title: 'Loading...',
      allowOutsideClick: false,
      showCancelButton: false,
      showConfirmButton: false,
      willOpen: () => {
        Swal.showLoading()
      },
    })
  }

  /////////////////////////////////////////////////////////////////////////////

  public takePicture() {
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    var video = <HTMLVideoElement>document.getElementById('video');
    canvas.setAttribute('width', video.videoWidth.toString());
    canvas.setAttribute('height', video.videoHeight.toString());
    ctx?.drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
    var data = canvas.toDataURL('image/jpeg', 1);
    // this.saveOnStackSwift(data);
    this.downloadImage(data)
  }

  async downloadImage(data: any) {
    let link: any;
    if (document.getElementById('linkDownload') === null) {
      link = document.createElement('a');
      link.href = data;
      link.setAttribute('id', 'linkDownload')
      link.style.display = 'none';
      document.body.appendChild(link);
    }
    const { value: name } = await Swal.fire({
      title: 'Enter name to image',
      input: 'text',
      showCancelButton: true,
      confirmButtonText: 'Confirm'
    })
    if (name) {
      link = document.getElementById('linkDownload')
      link.download = name + '.jpeg'
      link.click();
    }
    setTimeout(() => {
      document.body.removeChild(link);
      window.URL.revokeObjectURL(data);
    }, 100);
  }

  ///////////////////////////////////////////////////////////////////////

  public mouseOutVerify() {
    if (!this.isStopped) {
      this.isStopped = true;
      this.stopCamera('stop_move');
    }
  }

  public moveToScene(name: string, x: any, y: any, z: any) {
    this.cameraService.moveCamera(name, x, y, z, this.currentCamera.id)
  }

  public moveCamera(name: string, x: number, y: number, z: number) {
    this.isStopped = false;
    this.cameraService.moveCamera(name, x, y, z, this.currentCamera.id);
  }

  public stopCamera(name: string) {
    this.cameraService.stopCamera(name, this.currentCamera.id);
  }

  public zoomIn() {
    this.cameraService.zoomIn(this.currentCamera.id);
  }

  public zoomOut() {
    this.cameraService.zoomOut(this.currentCamera.id);
  }

  //////////////////////////////////////////////////////////////////////////////

  startRecording() {
    this.recordedBlobs = [];
    var video: any = <HTMLVideoElement>document.getElementById('video');
    // var stream = video?.captureStream() || video?.mozCaptureStream();
    const sUsrAg = navigator.userAgent;
    if (sUsrAg.indexOf('Firefox') > -1) {
      var stream = video.mozCaptureStream();
    } else {
      var stream = video.captureStream();
    }
    try {
      this.mediaRecorder = new MediaRecorder(stream, {
        mimeType: 'video/webm',
      });
    } catch (e) {
      console.error('Exception while creating MediaRecorder:', e);
      return;
    }
    this.mediaRecorder.ondataavailable = (event: any) => {
      if (event.data && event.data.size > 0) {
        this.recordedBlobs.push(event.data);
        // this.saveVideoSwift(event.data);
      }
    };
    this.mediaRecorder.start();
    this.isRecording = true;
    this.isFinishRecord = false;
    this.setTimeoutId = setTimeout(() => {
      this.stopRecording();
      Swal.fire({
        icon: 'warning',
        text: 'La grabación ha alcanzado el tiempo limite!!'
      })
    }, 60000);
  }

  stopRecording() {
    clearTimeout(this.setTimeoutId);
    this.mediaRecorder.stop();
    this.isRecording = false;
    this.isFinishRecord = true;
  }

  playRecording() {
    document.getElementById('myModal')?.classList.toggle('none');
    var modal = document.getElementById('myModal');
    var modalVideo = <HTMLVideoElement>document.getElementById('recorded');
    const videoBuffer = new Blob(this.recordedBlobs, { type: 'video/mp4' });
    this.downloadUrl = window.URL.createObjectURL(videoBuffer);
    this.recorded.nativeElement.src = this.downloadUrl;
    this.recorded.nativeElement.controls = true;
  }

  closeModalVideo() {
    document.getElementById('myModal')?.classList.toggle('none');
  }

  async downloadRecording() {
    document.getElementById('recorded')?.classList.add('none');
    const blob = new Blob(this.recordedBlobs, { type: 'video/mp4' });
    const url = window.URL.createObjectURL(blob);
    let link: any;
    if (document.getElementById('linkDownload') === null) {
      link = document.createElement('a');
      link.href = url;
      link.setAttribute('id', 'linkDownload')
      link.style.display = 'none';
      document.body.appendChild(link);
    }
    const { value: name } = await Swal.fire({
      title: 'Enter name to video file',
      input: 'text',
      showCancelButton: true,
      confirmButtonText: 'Confirm'
    })
    if (name) {
      link = document.getElementById('linkDownload')
      link.download = name + '.mp4'
      link.click();
    }
  }

  ////////////////////////////////////////////////////////////////////////////////////

  saveVideoSwift(data: any) {
    const blob = new Blob(this.recordedBlobs, { type: 'video/mp4' });
    var reader = new FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = (e) => {
      this.base64 = reader.result;
      this.saveOnStackSwift(this.base64);
    };
  }

  saveOnStackSwift(base64: string) {
    this.openStackService.saveOnOpenStack(this.id, this.currentCamera.reference, base64);
  }

  public async newPosition() {
    const { value: name } = await Swal.fire({
      title: 'Enter name to new scene',
      input: 'text',
      showCancelButton: true,
      confirmButtonText: 'Confirm'
    })
    if (name) {
      console.log('name is : ', name);
    }
  }
}