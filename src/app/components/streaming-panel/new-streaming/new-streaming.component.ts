import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Stream } from 'src/app/models/stream';
import { StreamService } from 'src/app/services/stream.service';

@Component({
  selector: 'app-new-streaming',
  templateUrl: './new-streaming.component.html',
  styleUrls: ['./new-streaming.component.scss'],
})
export class NewStreamingComponent implements OnInit {
  idCamera: any;
  streamingForm: FormGroup;
  currentStream: Stream;
  constructor(
    private streamService: StreamService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.idCamera = this.route.snapshot.paramMap.get('id');
    this.streamingForm = this.createFormGroup();
  }
  createFormGroup() {
    return this.formBuilder.group({
      protocol: new FormControl('', [Validators.required]),
      active: new FormControl('', [Validators.required]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
      url: new FormControl('', [Validators.required, Validators.minLength(4)]),
      user: new FormControl(''),
      password: new FormControl(''),
      video_url: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
      ]),
    });
  }

  onSubmit() {
    if (this.streamingForm.valid) {
      this.setCurrentStream();
      this.streamService.newStream(this.currentStream).subscribe((res) => {
      });
      this.streamingForm.reset();
      this.router.navigate(['streaming-dashboard/' + this.idCamera]);
    }
  }

  closeForm() {
    this.router.navigate(['streaming-dashboard/' + this.idCamera]);
  }

  setCurrentStream() {
    let data = JSON.stringify({
      user: this.form.user.value,
      password: this.form.password.value,
      video_url: this.form.video_url.value,
    });
    this.currentStream = {
      active: this.form.active.value,
      camera_id: this.idCamera,
      description: this.form.description.value,
      id: 0,
      url: this.form.url.value,
      protocol_id: this.form.protocol.value,
      data: data,
    };
  }

  get form() {
    return this.streamingForm.controls;
  }
}
