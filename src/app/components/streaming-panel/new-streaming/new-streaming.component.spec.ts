import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewStreamingComponent } from './new-streaming.component';

describe('NewStreamingComponent', () => {
  let component: NewStreamingComponent;
  let fixture: ComponentFixture<NewStreamingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewStreamingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewStreamingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
