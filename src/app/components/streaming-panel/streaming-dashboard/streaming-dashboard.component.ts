import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { StreamService } from 'src/app/services/stream.service';
import { Stream } from '../../../models/stream';

@Component({
  selector: 'app-streaming-dashboard',
  templateUrl: './streaming-dashboard.component.html',
  styleUrls: ['./streaming-dashboard.component.scss'],
})
export class StreamingDashboardComponent implements OnInit {
  title = 'Lista de streamings';
  idCamera: string;
  displayedColumns: string[] = [
    '#',
    'URL',
    'Protocol',
    'Description',
    'Active',
    'Actions',
  ];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private streamService: StreamService
  ) {}

  ngOnInit(): void {
    this.idCamera = this.route.snapshot.paramMap.get('id');
    this.dataSource = new MatTableDataSource();
    this.loadTable();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  loadTable() {
    this.streamService
      .getStreams(this.idCamera)
      .subscribe((stream: Stream[]) => {
        this.dataSource.data = stream;
      });
  }

  editStream(id: number) {
    this.router.navigate(['edit-streaming/' + id]);
  }

  deleteStream(id: string) {
    this.streamService.deleteStream(id).subscribe((res) => {
    });
    this.loadTable();
  }

  addStream() {
    this.router.navigate(['new-streaming/' + this.idCamera]);
  }
}
