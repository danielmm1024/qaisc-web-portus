import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void { }

  public isLogin() {
    return this.authService.isLogged() == null ? false : true;
  }

  onSignOut() {
    localStorage.removeItem('user')
    this.router.navigate(['']);
  }


  goToLogin() {
    this.router.navigate(['login']);
  }

  goToCameras() {
    this.router.navigate(['cameras']);
  }

  goToMap() {
    this.router.navigate(['']);
  }
}
