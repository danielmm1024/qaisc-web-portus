import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Camera } from 'src/app/models/camera';
import { CameraService } from 'src/app/services/camera.service';

@Component({
  selector: 'app-camera-details',
  templateUrl: './camera-details.component.html',
  styleUrls: ['./camera-details.component.scss'],
})
export class CameraDetailsComponent implements OnInit {
  isEditing = false;
  id: any;
  currentCamera: Camera;
  public actives: Array<any> = [{value: 0, name: "true"}, {value: 1, name: "false"}]
  selectedActive: any = "true"

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cameraService: CameraService
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getCamera(this.id);
  }

  getCamera(id: string) {
    this.cameraService.getCamera(this.id).subscribe((camera: Camera) => {
      this.currentCamera = camera;
      // this.selectedActive = this.currentCamera.active
    });
  }

  updateCamera() {
    this.cameraService.updateCamera(this.currentCamera).subscribe((res) => {});
    this.router.navigate(['cameras']);
  }

  closeForm() {
    this.router.navigate(['cameras']);
  }

  compare(a: any, b: any){
    return true;
  }
}
