import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Camera } from 'src/app/models/camera';
import { CameraService } from 'src/app/services/camera.service';

@Component({
  selector: 'app-new-camera',
  templateUrl: './new-camera.component.html',
  styleUrls: ['./new-camera.component.scss'],
})
export class NewCameraComponent implements OnInit {
  currentCamera: Camera = {
    id: 7,
    name: '',
    active: true,
    azimuth: 0,
    dd_latitude: 0,
    dd_longitude: 0,
    dds_latitude: '0',
    dds_longitude: '0',
    host: '',
    onvif_pass: '',
    onvif_user: '',
    port: 0,
    project_id: 0,
    reference: '',
    version: 1,
  };
  cameraForm!: FormGroup;

  constructor(
    private router: Router,
    private cameraService: CameraService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.cameraForm = this.createFormGroup();
  }

  newCamera() {
    if (this.cameraForm.valid) {
      this.setCurrentCamera();
      this.cameraService.storeCamera(this.currentCamera).subscribe((res) => {});
      this.cameraForm.reset();
      this.router.navigate(['cameras']);
    }
  }

  closeForm() {
    this.router.navigate(['cameras']);
  }

  createFormGroup() {
    return this.formBuilder.group({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      reference: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      active: new FormControl(''),
      version: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      host: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      port: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      azimuth: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      onvif_user: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      onvif_pass: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      dd_latitude: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      dd_longitude: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      dds_latitude: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
      dds_longitude: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(50),
      ]),
    });
  }

  setCurrentCamera() {
    this.currentCamera = {
      name: this.form.name.value,
      active: this.form.active.value,
      azimuth: this.form.azimuth.value,
      dd_latitude: this.form.dd_latitude.value,
      dd_longitude: this.form.dd_longitude.value,
      dds_latitude: this.form.dds_latitude.value,
      dds_longitude: this.form.dds_longitude.value,
      host: this.form.host.value,
      id: 0,
      onvif_pass: this.form.onvif_pass.value,
      onvif_user: this.form.onvif_user.value,
      port: this.form.port.value,
      project_id: 0,
      reference: this.form.reference.value,
      version: this.form.version.value,
    };
  }

  get form() {
    return this.cameraForm.controls;
  }
}
