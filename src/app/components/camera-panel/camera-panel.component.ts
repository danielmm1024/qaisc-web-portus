import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Camera } from 'src/app/models/camera';
import { CameraService } from 'src/app/services/camera.service';

@Component({
  selector: 'app-camera-panel',
  templateUrl: './camera-panel.component.html',
  styleUrls: ['./camera-panel.component.scss'],
})
export class CameraPanelComponent implements OnInit, AfterViewInit {
  title = 'Lista de cámaras';
  displayedColumns: string[] = [
    '#',
    'name',
    'host',
    'port',
    'active',
    'actions',
  ];
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private cameraService: CameraService
  ) {}

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
    this.loadTable();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  loadTable() {
    this.cameraService.getCamerasJSON().subscribe((cameras: Camera[]) => {
      this.dataSource.data = cameras;
    });
  }

  openStreaming(id: number) {
    this.router.navigate(['streaming/' + id]);
  }

  editCamera(id: number) {
    this.router.navigate(['camera/' + id]);
  }

  addCamera() {
    this.router.navigate(['new-camera/']);
  }

  deleteCamera(id: number) {
    this.cameraService.deleteCamera(id).subscribe((res) => {});
    this.loadTable();
  }

  addStreaming(id: number) {
    this.router.navigate(['streaming-dashboard/' + id]);
  }
}
