export interface Camera {
  id: number;
  name: string;
  reference: string;
  project_id: number;
  host: string;
  port: number;
  version: number;
  azimuth: number;
  onvif_pass: string;
  onvif_user: string;
  dd_longitude: number;
  dd_latitude: number;
  dds_longitude: string;
  dds_latitude: string;
  active: boolean;
}
