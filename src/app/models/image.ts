export interface Image {
  id: number;
  name: string;
  container: string;
  hash: string;
  taken_at: string;
  camera: string;
}
