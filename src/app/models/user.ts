export interface User {
  username: string;
  email: string;
  password: string;
  c_password: string;
}
