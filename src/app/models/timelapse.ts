export interface Timelapse {
  id: number,
  name: string,
  camera_id: number,
  task_id: number,
}