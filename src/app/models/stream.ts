export interface Stream {
  id: number;
  url: string;
  description: string;
  camera_id: number;
  active: boolean;
  protocol_id: number;
  data: any;
}
