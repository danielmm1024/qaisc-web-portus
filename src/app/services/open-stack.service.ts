import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Image } from '../models/image';

@Injectable({
  providedIn: 'root',
})
export class OpenStackService {
  constructor(private http: HttpClient) { }

  saveOnOpenStack(id: number, name: string, base64: string) {
    var url = environment.URL_API + '/api/v1/catalogue/camera/file/' + id;
    var base64code = base64.split(',').pop();
    var action = {
      name: name + '_' + new Date().toISOString(),
      base64: base64code,
    };
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    this.http.post(url, action, httpOptions).subscribe();
  }

  getImages(id: number, limit: number): Observable<Image[]> {
    var url = environment.URL_API + '/api/v1/catalogue/camera/file';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
      params: { camera_id: id.toString(), limit: limit.toString() },
    };
    return this.http.get<Image[]>(url, httpOptions);
  }

  getImagesByScene(id: number): Observable<Image[]> {
    var url = environment.URL_API + '/api/v1/catalogue/camera/file/task/' + id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    return this.http.get<Image[]>(url, httpOptions);
  }
}
