import { TestBed } from '@angular/core/testing';

import { OpenStackService } from './open-stack.service';

describe('OpenStackService', () => {
  let service: OpenStackService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpenStackService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
