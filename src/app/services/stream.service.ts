import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Stream } from '../models/stream';
import { Observable } from 'rxjs';
import { Timelapse } from '../models/timelapse';

declare function loadWebRtc(url: any, video_url: any): any;
declare function loadWebRtc2(url: any, video_url: any, div: any): any;
@Injectable({
  providedIn: 'root',
})
export class StreamService {
  url: string;
  video_url: any;

  constructor(private http: HttpClient) { }

  getStream(id: number): Observable<Stream> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    var url = environment.URL_API + '/api/v1/stream/' + id;
    return this.http.get<Stream>(url, httpOptions);
  }

  getStreamById(id: number): Observable<Stream> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    var url = environment.URL_API + '/api/v1/stream/id/' + id;
    return this.http.get<Stream>(url, httpOptions);
  }

  loadStreaming(stream: Stream) {
    var aux = JSON.parse(stream.data);
    var video_url = aux['video_url'];
    loadWebRtc(stream.url, video_url);
  }

  loadStreaming2(stream: Stream) {
    var aux = JSON.parse(stream.data);
    var video_url = aux['video_url'];
    loadWebRtc2(stream.url, video_url, 'video' + stream.camera_id);
  }

  getStreams(id: string): Observable<Stream[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    var url = environment.URL_API + '/api/v1/stream/index/' + id;
    return this.http.get<Stream[]>(url, httpOptions);
  }

  getStreamEdit(id: string): Observable<Stream> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    var url = environment.URL_API + '/api/v1/stream/edit/' + id;
    return this.http.get<Stream>(url, httpOptions);
  }

  newStream(stream: Stream) {
    var url = environment.URL_API + '/api/v1/stream/store';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    return this.http.post(url, stream, httpOptions);
  }

  deleteStream(id: string) {
    var url = environment.URL_API + '/api/v1/stream/delete/' + id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    return this.http.post(url, id, httpOptions);
  }

  updateStream(stream: Stream): Observable<any> {
    var url = environment.URL_API + '/api/v1/stream/update/' + stream.id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    return this.http.put(url, stream, httpOptions);
  }

  getTimelapses(id: number): Observable<Timelapse[]> {
    var url = environment.URL_API + '/api/v1/timelapse/' + id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('user')!,
      }),
    };
    return this.http.get<Timelapse[]>(url, httpOptions);
  }

  getPositions(id: any): Observable<any> {
    var url = environment.URL_API + '/api/v1/position/' + id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    return this.http.get(url, httpOptions);
  }
}
