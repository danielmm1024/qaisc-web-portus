import { HttpClient } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  user: any;
  token?: any;
  res: any;
  currentUser: any;

  constructor(
    private router: Router,
    private ngZone: NgZone,
    private http: HttpClient
  ) {}

  async signIn(name: string, password: string) {
    return new Promise((resolve, reject) => {
      this.http
        .post('http://192.168.22.12:8002/api/v1/auth/login', {
          name: name,
          password: password,
        })
        .subscribe(
          (response) => {
            resolve(response);
            this.ngZone.run(() => {
              this.setCurrentUser(response);
              localStorage.setItem('user', this.token);
              this.router.navigate(['/map']);
            });
          },
          (err) => {
            reject(err);
          }
        );
    });
  }

  async signUp(user: User) {
    return new Promise((resolve, reject) => {
      this.http.post('http://192.168.22.12:8002/api/v1/user/', user).subscribe(
        (response) => {
          resolve(response);
          this.ngZone.run(() => {
            this.setCurrentUser(response);
            this.router.navigate(['/login']);
          });
        },
        (err) => {
          reject(err);
        }
      );
    });
  }

  signOut() {
    this.token = null;
    localStorage.removeItem('user');
    localStorage.removeItem('username');
    this.router.navigate(['/login']);
  }

  setCurrentUser(token: any) {
    this.token = token.success.token;
  }

  isLogged() {
    return localStorage.getItem('user');
  }
}
