import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Camera } from '../models/camera';
import { Stream } from '../models/stream';
import { StreamService } from './stream.service';

declare function loadWebRtc(url: any, video_url: any): any;
@Injectable({
  providedIn: 'root',
})
export class CameraService {
  url: any;
  video_url: any;
  currentCamera: any;
  v: number = 0.4;
  base64code: string;
  cameras: Camera[] = [];
  camera: Camera;
  Observable: Observable<Camera[]>;

  constructor(private http: HttpClient, private streamService: StreamService) { }

  getCamerasJSON(): Observable<Camera[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    var url = environment.URL_API + '/api/v1/camera';
    return this.http.get<Camera[]>(url, httpOptions);
  }

  getCamera(id: number): Observable<Camera> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    var url = environment.URL_API + '/api/v1/camera/' + id;
    return this.http.get<Camera>(url, httpOptions);
  }

  moveToScene(id: number) {
    var action = {
      'action[]': JSON.stringify({
        name: 'move',
        x: '0.6',
        y: '0.5',
        z: '0',
      }),
    };
    var url = environment.URL_API + '/api/v1/message/' + id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
      params: action,
    };
    this.http.post(url, null, httpOptions).subscribe();
  }

  moveCamera(name: string, x: number, y: number, z: number, id: number) {
    var action = {
      'action[]': JSON.stringify({
        name: name,
        x: x.toString(),
        y: y.toString(),
        z: z.toString(),
      }),
    };
    var url = environment.URL_API + '/api/v1/message/' + id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
      params: action,
    };
    this.http.post(url, null, httpOptions).subscribe();
  }

  stopCamera(name: string, id: number) {
    var action = { 'action[]': JSON.stringify({ name: name }) };
    var url = environment.URL_API + '/api/v1/message/' + id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
      params: action,
    };
    this.http.post(url, null, httpOptions).subscribe();
  }

  zoomIn(id: number) {
    this.moveCamera('continuous_move', 0, 0, this.v, id);
    this.stopCamera('stop_move', id);
  }

  zoomOut(id: number) {
    this.moveCamera('continuous_move', 0, 0, -this.v, id);
    this.stopCamera('stop_move', id);
  }

  updateCamera(camera: Camera): Observable<any> {
    var url = environment.URL_API + '/api/v1/camera/update/' + camera.id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    return this.http.put(url, camera, httpOptions);
  }

  deleteCamera(id: number) {
    var url = environment.URL_API + '/api/v1/camera/delete/' + id;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    return this.http.post(url, id, httpOptions);
  }

  storeCamera(camera: Camera) {
    var url = environment.URL_API + '/api/v1/camera/store';
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + environment.TOKEN_API,
      }),
    };
    return this.http.post(url, camera, httpOptions);
  }

  loadCameras() {
    this.getCamerasJSON().subscribe((cameras: Camera[]) => {
      cameras.forEach((camera: Camera) => {
        this.cameras.push(this.setCamera(camera));
        if (camera.active) {
          this.createVideoStreaming(camera.id);
          this.loadWebRtcStream2(camera.id);
        }
      });
      this.cameras = cameras;
    });
    return this.cameras;
  }

  async loadCameras2() {
    return new Promise((resolve, reject) => {
      this.getCamerasJSON().subscribe(
        (cameras: Camera[]) => {
          cameras.forEach((camera: Camera) => {
            this.cameras.push(this.setCamera(camera));
            if (camera.active) {
              this.createVideoStreaming(camera.id);
              this.loadWebRtcStream2(camera.id);
            }
          });
          resolve(this.cameras);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  getCamerasObserver(): any {
    this.getCamerasJSON().subscribe((cameras: Camera[]) => {
      cameras.forEach((camera: Camera) => {
        this.cameras.push(this.setCamera(camera));
        if (camera.active) {
          this.createVideoStreaming(camera.id);
          this.loadWebRtcStream2(camera.id);
        }
      });
    });
    var cameraObservable = new Observable((observer) => {
      observer.next(this.cameras);
    });

    return cameraObservable;
  }

  setCamera(camera: Camera) {
    this.camera = {
      id: camera.id,
      name: camera.name,
      dd_longitude: camera.dd_longitude,
      dds_longitude: camera.dds_longitude,
      dd_latitude: camera.dd_latitude,
      dds_latitude: camera.dds_latitude,
      active: camera.active,
      reference: camera.reference,
      project_id: camera.project_id,
      host: camera.host,
      port: camera.port,
      version: camera.version,
      azimuth: camera.azimuth,
      onvif_pass: camera.onvif_pass,
      onvif_user: camera.onvif_user,
    };
    return this.camera;
  }

  createVideoStreaming(id: any) {
    var video = document.createElement('video');
    video.setAttribute('id', 'video' + id);
    video.setAttribute('width', '300');
    video.setAttribute('autoplay', 'true');
    video.style.display = 'none';
    document.getElementById('videoStreaming').appendChild(video);
  }

  loadWebRtcStream(id: number) {
    this.streamService.getStream(id).subscribe((stream: Stream) => {
      this.streamService.loadStreaming(stream);
    });
  }

  loadWebRtcStream2(id: number) {
    this.streamService.getStream(id).subscribe((stream: Stream) => {
      this.streamService.loadStreaming2(stream);
    });
  }
}
