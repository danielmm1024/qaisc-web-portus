import { Injectable } from '@angular/core';
import Feature from 'ol/Feature';
import * as olProj from 'ol/proj';
import Point from 'ol/geom/Point';
import { Camera } from '../models/camera';
import { Icon, Style } from 'ol/style';
import VectorLayer from 'ol/layer/Vector';
import VectorSource from 'ol/source/Vector';

@Injectable({
  providedIn: 'root',
})
export class CameraGeneratorService {
  vectorLayer: VectorLayer;

  constructor() {}

  createLayer(camera: Camera) {
    var iconFeature = new Feature({
      geometry: new Point(
        olProj.fromLonLat([camera.dd_longitude, camera.dd_latitude])
      ),
      name: 'streaming/' + camera.id,
    });
    var iconStyle;
    iconStyle = new Style({
      image: new Icon({
        src: camera.active
          ? 'assets/imgs/camera_on.png'
          : 'assets/imgs/camera_off.png',
        scale: [0.6, 0.6],
      }),
    });
    iconFeature.setStyle(iconStyle);
    this.vectorLayer = new VectorLayer({
      source: new VectorSource({
        features: [iconFeature],
      }),
    });
    return this.vectorLayer;
  }
}
