import { TestBed } from '@angular/core/testing';

import { CameraGeneratorService } from './camera-generator.service';

describe('CameraGeneratorService', () => {
  let service: CameraGeneratorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CameraGeneratorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
