import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IvyCarouselModule } from 'angular-responsive-carousel';
// services
import { CameraService } from './services/camera.service';
import { StreamService } from './services/stream.service';
import { OpenStackService } from './services/open-stack.service';
// angular material
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSliderModule } from '@angular/material/slider';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
//Componentes
import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { StreamingPanelComponent } from './components/streaming-panel/streaming-panel.component';
import { CameraPanelComponent } from './components/camera-panel/camera-panel.component';
import { CameraDetailsComponent } from './components/camera-panel/camera-details/camera-details.component';
import { NewCameraComponent } from './components/camera-panel/new-camera/new-camera.component';
import { NewUserComponent } from './components/new-user/new-user.component';
import { NewStreamingComponent } from './components/streaming-panel/new-streaming/new-streaming.component';
import { StreamingDashboardComponent } from './components/streaming-panel/streaming-dashboard/streaming-dashboard.component';
import { EditStreamingComponent } from './components/streaming-panel/edit-streaming/edit-streaming.component';

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    CameraPanelComponent,
    LoginComponent,
    NavbarComponent,
    StreamingPanelComponent,
    CameraDetailsComponent,
    NewCameraComponent,
    NewUserComponent,
    NewStreamingComponent,
    StreamingDashboardComponent,
    EditStreamingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    MatSliderModule,
    MatTableModule,
    MatMenuModule,
    MatSelectModule,
    IvyCarouselModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatDialogModule,
    ReactiveFormsModule,
  ],
  providers: [CameraService, StreamService, OpenStackService],
  bootstrap: [AppComponent],
})
export class AppModule { }
