import { Component, OnInit, AfterViewInit } from '@angular/core';
import OlMap from 'ol/Map';
import OlView from 'ol/View';
import OSM from 'ol/source/OSM';
import * as Proj from 'ol/proj';
import 'ol/ol.css';
import Overlay from 'ol/Overlay';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';

import { Camera } from '../models/camera';
import { CameraService } from '../services/camera.service';
import { Router } from '@angular/router';
import { CameraGeneratorService } from '../services/camera-generator.service';
declare function unLoadWebRtc(): any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements AfterViewInit, OnInit {
  map: OlMap;
  view: OlView;
  layer: TileLayer;
  vectorLayer: VectorLayer;
  camerasLayers: any[] = [];
  cameras: Camera[] = [];
  camera: Camera;
  url: any;
  video_url: any;
  currentCamera: Camera;
  idCamera: number;

  container: any;
  content: any;
  closer: any;
  selectedFeature: any = null;
  overlay: Overlay;

  constructor(
    private cameraService: CameraService,
    private cameraGeneratorService: CameraGeneratorService,
    private router: Router
  ) { }

  ngOnInit() {
    // this.createVideoStreaming();
  }

  ngAfterViewInit(): void {
    this.layer = new TileLayer({
      source: new OSM(),
    });
    this.view = new OlView({
      center: Proj.fromLonLat([-15.6, 27.95]),
      zoom: 10,
      minZoom: 2,
      // maxZoom: 14,
    });
    this.overlay = new Overlay({
      element: document.getElementById('popup'),
      autoPan: false,
    });
    this.map = new OlMap({
      target: 'map',
      layers: [this.layer],
      view: this.view,
      overlays: [this.overlay],
    });
    // this.getCamerasJson();
    this.initCameras();
    this.map.on('click', (event: any) =>
      this.map.forEachFeatureAtPixel(
        event.pixel,
        (feature) => this.openCameraPanel(feature, event),
        { hitTolerance: 5 }
      )
    );
    this.map.on('pointermove', (event: any) => this.hoverCamera(event));
  }

  initCameras() {
    this.cameras = this.cameraService.loadCameras();
    setTimeout(() => {
      this.addLayerCameras(this.cameras);
    }, 1000);
  }

  addLayerCameras(cameras: Camera[]) {
    cameras.forEach((camera) => {
      this.map.addLayer(this.cameraGeneratorService.createLayer(camera));
    });
  }

  openCameraPanel(feature: any, event: any) {
    this.cameraService.getCamera(feature.get('name').split('/').pop())
    // this.router.ngOnDestroy();
    this.router.navigate([feature.get('name')])
  }

  hoverCamera(event: any) {
    var hover = this.map.forEachFeatureAtPixel(
      event.pixel,
      (feature) => {
        this.selectedFeature = feature;
        return true;
      },
      { hitTolerance: 5 }
    );
    hover ? this.openCameraPopup2(event) : this.closePopup();
  }

  openCameraPopup2(event: any) {
    this.idCamera = this.selectedFeature.get('name').split('/').pop();
    this.content = document.getElementById('popup-content');
    if (this.overlay.getPosition() == undefined) {
      this.cameraService.getCamera(this.idCamera).subscribe((camera: Camera) => {
        this.currentCamera = camera;
        if (camera.active) {
          this.showVideo(camera);
          this.overlay.setPosition(
            Proj.fromLonLat([
              this.currentCamera.dd_longitude,
              this.currentCamera.dd_latitude,
            ])
          );
        } else {
          // this.content!.innerHTML =
          //   '<img id="video" src="assets/imgs/notworking.jpg" width="200" height="auto">';
        }
      });
    }
  }

  showVideo(camera: Camera) {
    if (this.content.firstChild == null) {
      this.content.appendChild(document.getElementById('video' + camera.id));
      document.getElementById('video' + camera.id).style.display = '';
    }
  }

  closePopup(): void {
    if (this.overlay.getPosition() !== undefined) {
      this.overlay.setPosition(undefined);
      this.currentCamera.name = '';
      if (this.currentCamera.active) {
        this.content.firstChild!.style.display = 'none';
        document.getElementById('videoStreaming').appendChild(this.content.firstChild);
      }
    }
  }
}
