import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StreamingPanelComponent } from './components/streaming-panel/streaming-panel.component';
import { LoginComponent } from './components/login/login.component';
import { MapComponent } from './map/map.component';
import { CameraPanelComponent } from './components/camera-panel/camera-panel.component';
import { CameraDetailsComponent } from './components/camera-panel/camera-details/camera-details.component';
import { NewCameraComponent } from './components/camera-panel/new-camera/new-camera.component';
import { NewUserComponent } from './components/new-user/new-user.component';
import { NewStreamingComponent } from './components/streaming-panel/new-streaming/new-streaming.component';
import { StreamingDashboardComponent } from './components/streaming-panel/streaming-dashboard/streaming-dashboard.component';
import { EditStreamingComponent } from './components/streaming-panel/edit-streaming/edit-streaming.component';
import { AuthGuard } from "./guard/auth.guard";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'streaming/:id', component: StreamingPanelComponent, canActivate: [AuthGuard] },
  { path: 'streaming-dashboard/:id', component: StreamingDashboardComponent, canActivate: [AuthGuard] },
  { path: 'new-streaming/:id', component: NewStreamingComponent, canActivate: [AuthGuard] },
  { path: 'edit-streaming/:id', component: EditStreamingComponent, canActivate: [AuthGuard] },
  { path: 'camera/:id', component: CameraDetailsComponent, canActivate: [AuthGuard] },
  { path: 'new-camera', component: NewCameraComponent, canActivate: [AuthGuard] },
  { path: 'new-user', component: NewUserComponent, canActivate: [AuthGuard] },
  { path: 'cameras', component: CameraPanelComponent, canActivate: [AuthGuard] },
  { path: '**', component: MapComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
