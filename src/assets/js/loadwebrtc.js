var webRtcServer = null;

function loadWebRtc(url, video_url) {
    webRtcServer = new WebRtcStreamer("video", url);
    webRtcServer.connect(video_url);
}

function unLoadWebRtc() {
  webRtcServer.disconnect();
}

function loadWebRtc2(url, video_url, div) {
  webRtcServer = new WebRtcStreamer(div, url);
  webRtcServer.connect(video_url);
}
